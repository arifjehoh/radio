/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:53.
 * Last modified 2019-06-14 12:25.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.di

import androidx.lifecycle.ViewModel
import dagger.MapKey
import kotlin.annotation.Target
import kotlin.annotation.Retention
import kotlin.reflect.KClass
import kotlin.annotation.MustBeDocumented


@MustBeDocumented
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)
