/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:53.
 * Last modified 2019-06-14 12:25.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.arifjehdaooh.radio.func_channel.ChannelViewModel
import com.arifjehdaooh.radio.func_main.MainViewModel
import com.arifjehdaooh.radio.viewmodel.ProjectViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChannelViewModel::class)
    abstract fun bindChannelViewModel(viewModel: ChannelViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ProjectViewModelFactory): ViewModelProvider.Factory
}
