/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:53.
 * Last modified 2019-06-13 12:29.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class ApplicationModule {
    private var application: Application? = null

    constructor(application: Application?) {
        this.application = application
    }


    @Provides
    @Singleton
    fun provideContext(): Context? {
        return application
    }
}
