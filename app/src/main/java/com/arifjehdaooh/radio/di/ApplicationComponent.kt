/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:53.
 * Last modified 2019-06-14 12:25.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.di

import com.arifjehdaooh.radio.ProjectApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

import javax.inject.Singleton

@Component(modules = [
    AndroidSupportInjectionModule::class,
    ApplicationModule::class,
    NetworkModule::class,
    ActivityBuilderModule::class])
@Singleton
interface ApplicationComponent: AndroidInjector<ProjectApplication> {
    @Component.Builder
    abstract class Builder: AndroidInjector.Builder<ProjectApplication>() {
        abstract fun applicationModule(applicationModule: ApplicationModule): Builder
    }
}