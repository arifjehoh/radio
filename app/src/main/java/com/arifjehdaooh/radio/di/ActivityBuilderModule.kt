/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:53.
 * Last modified 2019-06-13 12:29.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.di

import com.arifjehdaooh.radio.func_channel.ChannelActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.arifjehdaooh.radio.func_main.MainActivity

@Module
abstract class ActivityBuilderModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeMainActivityInjector(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeChannelActivityInjector(): ChannelActivity
}
