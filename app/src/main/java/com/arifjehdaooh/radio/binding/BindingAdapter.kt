/*
 * Developed by Arif Jehda-Oh on 2019-07-10 14:08.
 * Last modified 2019-07-10 14:08.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.binding

import android.view.View
import android.view.View.*
import androidx.databinding.BindingAdapter

object BindingAdapter {
    
    @BindingAdapter("visibleGone")
    @JvmStatic
    fun setVisibleGone(view: View, show: Boolean) {
        view.visibility = if (show) VISIBLE else GONE
    }
}