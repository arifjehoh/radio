/*
 * Developed by Arif Jehda-Oh on 2019-06-20 15:56.
 * Last modified 2019-06-20 15:56.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.api.entity

class Page(
    var page: Int,
    var size: Int,
    var totalhits: Int,
    var totalpages: Int) {
}