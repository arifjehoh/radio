/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:52.
 * Last modified 2019-06-14 12:25.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.api.entity

import org.json.JSONException
import org.json.JSONObject
import org.json.JSONTokener

class ErrorBody(val errorCode: Int) {
    companion object {

        fun fromString(errorBody: String?): ErrorBody {
            if (errorBody == null) {
                throw JSONException("The errorBody is null")
            }

            var objectToken: Any? = JSONTokener(errorBody).nextValue()// as? JSONObject ?: throw JSONException("The errorBody is not a json object:$errorBody")
            // TODO Remove if the line over works
            if (objectToken !is JSONObject) {
                throw JSONException("The errorBody is not a json object:$errorBody")
            }

            val errorObject: JSONObject = JSONTokener(errorBody).nextValue() as JSONObject
            val errorCode: Int = errorObject.getInt("ErrorCode")

            return ErrorBody(errorCode)
        }
    }
}