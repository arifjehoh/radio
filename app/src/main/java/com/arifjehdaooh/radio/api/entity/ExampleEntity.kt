/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:53.
 * Last modified 2019-06-14 12:25.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.api.entity

class ExampleEntity(
    var status: String?,
    var message: Map<String, List<String>>) {
}

