/*
 * Developed by Arif Jehda-Oh on 2019-06-20 15:26.
 * Last modified 2019-06-20 15:26.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.api.entity

import android.media.Image

class Channel(
    var image: String,
    var color: String,
    var tagline: String,
    var siteurl: String,
    var channeltype: String,
    var name: String,
    var id: String,
    var liveaudio: LiveAudio) {
}