/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:52.
 * Last modified 2019-06-14 12:25.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.api

import com.arifjehdaooh.radio.api.entity.ChannelEntity
import com.arifjehdaooh.radio.api.entity.ChannelsEntity
import com.arifjehdaooh.radio.api.entity.ExampleEntity
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

@SuppressWarnings("UnnecessaryInterfaceModifier")
interface NetworkService {

    @GET("channels?format=json")
    fun getChannels(): Call<ChannelsEntity>

    @GET("channels?format=json")
    fun getNextPage(@Query("page") page: String): Call<ChannelsEntity>

    @GET("channels/{id}?format=json")
    fun getChannel(@Path("id") id: String): Call<ChannelEntity>

    //Other
    //@GET("breeds/list/all")
    //fun getSomeExampleDogs(): Call<ExampleEntity>
}