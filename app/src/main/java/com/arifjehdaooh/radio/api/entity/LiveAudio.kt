/*
 * Developed by Arif Jehda-Oh on 2019-06-26 13:02.
 * Last modified 2019-06-26 13:02.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.api.entity

class LiveAudio(
    var id: String,
    var statkey: String,
    var url: String) {

}
