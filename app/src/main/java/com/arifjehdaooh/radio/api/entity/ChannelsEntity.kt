/*
 * Developed by Arif Jehda-Oh on 2019-06-20 15:14.
 * Last modified 2019-06-20 15:14.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.api.entity


class ChannelsEntity(
    var copyright: String,
    var channels: MutableList<Channel>,
    var pagination: Page) {
}