/*
 * Developed by Arif Jehda-Oh on 2019-06-26 12:41.
 * Last modified 2019-06-26 12:41.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.api.entity

class ChannelEntity(var channel: Channel, var copyright: String) {
}