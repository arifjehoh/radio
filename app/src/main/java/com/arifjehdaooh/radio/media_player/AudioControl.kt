/*
 * Developed by Arif Jehda-Oh on 2019-06-27 09:27.
 * Last modified 2019-06-27 09:27.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.media_player

import android.media.MediaPlayer
import android.widget.ImageView

class AudioControl {
    companion object {
        var image: String? = ""
        var name: Unit? = null
        var playing: Boolean? = false
        var mediaPlayer: MediaPlayer? = null
    }
}