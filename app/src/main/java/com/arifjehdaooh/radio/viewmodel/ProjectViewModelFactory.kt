/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:54.
 * Last modified 2019-06-14 12:25.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlin.Exception
import kotlin.IllegalArgumentException
import kotlin.RuntimeException
import javax.inject.Provider
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.Map

@Singleton
class ProjectViewModelFactory @Inject constructor(private val creators: Map<Class<out ViewModel>?,
        @JvmSuppressWildcards Provider<ViewModel>>) : ViewModelProvider.Factory{

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        var creator = creators[modelClass]
        if (creator == null) {
            for (entry in creators.entries) {
                if (modelClass.isAssignableFrom(entry.key)) {
                    creator = entry.value
                    break
                }
            }
        }
        if (creator == null) {
            throw IllegalArgumentException("unknown model class $modelClass")
        }
        try {
            return creator.get() as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }

}
