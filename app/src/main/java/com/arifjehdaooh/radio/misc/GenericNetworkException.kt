/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:53.
 * Last modified 2019-06-14 12:25.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.misc

class GenericNetworkException(val throwable: Throwable) : NetworkException() {
    override fun toString(): String {
        return throwable.toString()
    }
}