/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:53.
 * Last modified 2019-06-14 12:25.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.misc

import com.arifjehdaooh.radio.api.entity.ErrorBody
import org.json.JSONException
import retrofit2.Response
import timber.log.Timber
import java.io.IOException
import kotlin.Exception

public open class NetworkException() : Exception() {
    companion object {
        fun unknownError(): NetworkException {
            return GenericNetworkException(Exception())
        }

        fun create(response: Response<*>): NetworkException {
            //val host: String = response.raw().request().url().host()
            val errorResponseBody: okhttp3.ResponseBody? = response.errorBody()
            var errorBodyString: String? = null
            try{
                errorBodyString = errorResponseBody?.string()
                var errorBody: ErrorBody = ErrorBody.fromString(errorBodyString)
                return ApiNetworkException(response.code(), errorBody.errorCode)
            } catch (e: IOException) {
                Timber.v(Exception(errorBodyString, e), " RubyException body $errorBodyString")
                return GenericNetworkException(e)
            } catch (e: JSONException) {
                Timber.v(Exception(errorBodyString, e), " RubyException body $errorBodyString")
                return GenericNetworkException(e)
            }
        }

        fun create(throwable: Throwable): NetworkException {
            return GenericNetworkException(throwable)
        }
    }
}