/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:54.
 * Last modified 2019-06-14 12:25.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.func_main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.arifjehdaooh.radio.api.entity.ChannelsEntity
import com.arifjehdaooh.radio.api.entity.ExampleEntity
import com.arifjehdaooh.radio.repository.RadioRepository
import com.arifjehdaooh.radio.repository.Resource
import javax.inject.Inject

class MainViewModel @Inject constructor(var radioRepository: RadioRepository) : ViewModel() {

    fun getChannels(): LiveData<Resource<ChannelsEntity>> {
        return radioRepository.getChannels()
    }
    fun getNextPage(page: Int): LiveData<Resource<ChannelsEntity>> {
        return radioRepository.getNextPage(page)
    }
}