/*
 * Developed by Arif Jehda-Oh on 2019-06-26 12:37.
 * Last modified 2019-06-26 12:20.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.func_main

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arifjehdaooh.radio.BaseActivity
import com.arifjehdaooh.radio.R
import com.arifjehdaooh.radio.api.entity.Channel
import com.arifjehdaooh.radio.api.entity.ChannelsEntity
import com.arifjehdaooh.radio.api.entity.Page
import com.arifjehdaooh.radio.databinding.ActivityMainBinding
import com.arifjehdaooh.radio.func_channel.ChannelActivity
import com.arifjehdaooh.radio.media_player.AudioControl
import com.arifjehdaooh.radio.repository.Resource
import com.arifjehdaooh.radio.repository.Resource.Status.*
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import javax.inject.Inject
import com.sothree.slidinguppanel.SlidingUpPanelLayout.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.panel_audio.*

class MainActivity : BaseActivity(),
    ChannelAdapter.OnItemClickListener {
    companion object {
        const val CHANNEL_ID: String = "CHANNEL_ID"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var mBinding: ActivityMainBinding
    lateinit var mViewModel: MainViewModel
    lateinit var mPage: Page


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        mBinding.recyclerviewChannel.layoutManager = GridLayoutManager(this, 2)
        mBinding.adapter = ChannelAdapter()

        showChannels()

        // Audio control
        btn_play.setOnClickListener{
            AudioControl.mediaPlayer?.start()
        }
        btn_pause.setOnClickListener{
            AudioControl.mediaPlayer?.pause()
        }



        recyclerview_channel.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (mBinding.adapter!!.channels.isNotEmpty()) {
                    if (!recyclerView.canScrollVertically(1)) {
                        if (mPage.page <= mPage.totalpages) showNextPage(mPage.page+1)
                    }
                }
            }
        })
    }

    private fun showNextPage(page: Int) {
        mViewModel
            .getNextPage(page)
            .observe(this, Observer<Resource<ChannelsEntity>> { resource ->
                run {
                    when (resource.status) {
                        SUCCESS -> {
                            val entity = resource.data as ChannelsEntity
                            mPage = entity.pagination
                            for (channel in entity.channels) mBinding.adapter?.channels?.add(channel)
                            mBinding.adapter?.notifyDataSetChanged()
                        }
                        ERROR -> {
                        }
                        LOADING -> {
                            Timber.v("loading")
                        }
                    }
                }
            })
    }

    override fun onResume() {
        super.onResume()
        if (AudioControl.image?.isNotEmpty()!!) {
            Picasso.get().load(AudioControl.image).into(audio_image)
            Picasso.get().load(AudioControl.image).into(audio_image_small)
        }
    }

    private fun showChannels() {
        mViewModel
            .getChannels()
            .observe(this, Observer<Resource<ChannelsEntity>> { resource ->
                run {
                    when (resource.status) {
                        SUCCESS -> {
                            val entity: ChannelsEntity = resource.data as ChannelsEntity
                            mPage = entity.pagination
                            mBinding.adapter = ChannelAdapter(entity.channels,this)
                        }
                        ERROR -> {
                            Toast.makeText(this, "Error message: ${resource.exception}", Toast.LENGTH_SHORT).show()
                        }
                        LOADING -> {
                        }
                    }
                }
            })
    }

    override fun onItemClick(position: Int) {
        Timber.v("Position $position and is the audio playing? ${AudioControl.playing}")
        val intent = Intent(this, ChannelActivity::class.java)
        val bundle = Bundle()
        mBinding.adapter?.channels?.get(position)?.id?.toInt()?.let { bundle.putInt(CHANNEL_ID, it) }
        intent.putExtras(bundle)
        startActivity(intent)
    }

    override fun onBackPressed() {
        if (content_main != null &&
            content_main.panelState == PanelState.EXPANDED || content_main.panelState == PanelState.ANCHORED) {
            content_main.panelState = PanelState.COLLAPSED
        } else {
            super.onBackPressed()
        }
    }
}
