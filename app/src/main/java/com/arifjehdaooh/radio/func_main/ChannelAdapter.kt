/*
 * Developed by Arif Jehda-Oh on 2019-06-26 12:37.
 * Last modified 2019-06-26 11:53.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.func_main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arifjehdaooh.radio.api.entity.Channel
import com.arifjehdaooh.radio.databinding.RecyclerviewChannelBinding
import com.squareup.picasso.Picasso

class ChannelAdapter(): RecyclerView.Adapter<ChannelAdapter.ViewHolder>() {
    var channels: MutableList<Channel>
    lateinit var onItemClickListener: OnItemClickListener

    init {
        channels = mutableListOf()
    }
    
    constructor(channels: MutableList<Channel>, onItemClickListener: OnItemClickListener) : this() {
        this.channels = channels
        this.onItemClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RecyclerviewChannelBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding, onItemClickListener)
    }

    override fun getItemCount(): Int = channels.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(channels[position])


    class ViewHolder(
        private var binding: RecyclerviewChannelBinding,
        private var onItemClickListener: OnItemClickListener) : RecyclerView.ViewHolder(binding.root) {

        init {
            itemView.setOnClickListener { onItemClickListener.onItemClick(adapterPosition) }
        }

        fun bind(channel: Channel) {
            this.binding.channel = channel
            Picasso.get().load(channel.image).into(this.binding.logo)
        }

    }
}

