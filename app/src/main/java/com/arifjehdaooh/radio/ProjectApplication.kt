/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:54.
 * Last modified 2019-06-13 12:29.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio

import com.arifjehdaooh.radio.di.ApplicationModule
import com.arifjehdaooh.radio.di.DaggerApplicationComponent
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber

class ProjectApplication : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)
        setupTimber()
    }

    override protected fun applicationInjector(): AndroidInjector<ProjectApplication>? {
        return DaggerApplicationComponent.builder().applicationModule(
            ApplicationModule(
                this
            )
        ).create(this)
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}