/*
 * Developed by Arif Jehda-Oh on 2019-06-26 12:37.
 * Last modified 2019-06-26 12:31.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.func_channel

import android.graphics.Color
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.arifjehdaooh.radio.BaseActivity
import com.arifjehdaooh.radio.func_main.MainActivity
import com.arifjehdaooh.radio.R
import com.arifjehdaooh.radio.api.entity.ChannelEntity
import com.arifjehdaooh.radio.databinding.ActivityChannelBinding
import com.arifjehdaooh.radio.media_player.AudioControl
import com.arifjehdaooh.radio.repository.Resource
import timber.log.Timber
import javax.inject.Inject
import com.arifjehdaooh.radio.repository.Resource.Status.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_channel.*

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ChannelActivity : BaseActivity() {
    lateinit var mBinding: ActivityChannelBinding
    lateinit var mViewModel: ChannelViewModel
    lateinit var mediaPlayer: MediaPlayer

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_channel)
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(ChannelViewModel::class.java)

        if (intent.extras == null) {
            finish()
        }
        val id: Int = intent.extras.getInt(MainActivity.CHANNEL_ID)

        showChannel(id)

        btn_pause.setOnClickListener {
            if (AudioControl.mediaPlayer?.isPlaying!!) AudioControl.mediaPlayer?.pause()
            mBinding.playing = AudioControl.mediaPlayer?.isPlaying!!
        }
        btn_play.setOnClickListener{
            mediaPlayer = MediaPlayer().apply {
                setDataSource(baseContext, Uri.parse(mBinding.channel?.liveaudio?.url))
                prepare()
            }
            audioStop()
            AudioControl.mediaPlayer = mediaPlayer
            AudioControl.mediaPlayer?.start()
            AudioControl.image = mBinding.channel?.image
            mBinding.playing = AudioControl.mediaPlayer?.isPlaying!!
        }
    }

    private fun audioStop() {
        if (AudioControl.mediaPlayer != null) {
            if (AudioControl.mediaPlayer?.isPlaying!! && AudioControl.mediaPlayer != this.mediaPlayer) {
                AudioControl.mediaPlayer?.apply {
                    stop()
                    release()
                }
            }
        }
    }

    private fun showChannel(id: Int) {
        mViewModel
            .getChannel(id)
            .observe(this, Observer<Resource<ChannelEntity>> { resources ->
                when (resources.status) {
                    SUCCESS -> {
                        val entity = resources.data as ChannelEntity
                        mBinding.channel = entity.channel
                        Picasso.get().load(entity.channel.image).into(mBinding.imageLogo)
                        //mBinding.imageLogo.setBackgroundColor(Color.parseColor("#${entity.channel.color}"))
                    }
                    ERROR -> {
                    }
                    LOADING -> {
                    }
                }

            })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
