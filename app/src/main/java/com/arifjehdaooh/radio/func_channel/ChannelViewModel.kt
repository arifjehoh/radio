/*
 * Developed by Arif Jehda-Oh on 2019-06-26 12:37.
 * Last modified 2019-06-26 12:37.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.func_channel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.arifjehdaooh.radio.api.entity.ChannelEntity
import com.arifjehdaooh.radio.repository.RadioRepository
import com.arifjehdaooh.radio.repository.Resource
import javax.inject.Inject

class ChannelViewModel @Inject constructor(var radioRepository: RadioRepository) : ViewModel() {
    fun getChannel(channel_id: Int): LiveData<Resource<ChannelEntity>> {
        return radioRepository.getChannel(channel_id)
    }
}