/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:54.
 * Last modified 2019-06-14 12:25.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.repository

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.arifjehdaooh.radio.misc.NetworkException
import org.jetbrains.annotations.NotNull
import retrofit2.Response

/**
 * A generic class that holds a value with its loading status and possible errors.
 * @param <T> The type of object the resource will contain
 */
class Resource<T>(@NotNull val status: Status, @Nullable val data: Any?, @Nullable val exception: NetworkException?) {

    companion object {
        fun <T> success(@NonNull data: T): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> success(): Resource<T> {
            return Resource<T>(Status.SUCCESS, null, null)
        }
        fun <T> error(response: Response<*>): Resource<T> {
            return Resource(Status.ERROR, null, NetworkException.create(response))
        }

        fun <T> error(throwable: Throwable): Resource<T> {
            return Resource(Status.ERROR, null, NetworkException.create(throwable))
        }

        fun <T> error(resource: Resource<*>): Resource<T> {
            return Resource(Status.ERROR, null, resource.exception)
        }

        fun <T> error(): Resource<T> {
            return Resource(Status.ERROR, null, NetworkException.unknownError())
        }

        fun <T> loading(): Resource<T> {
            return Resource(Status.LOADING, null, null)
        }

    }

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }
}
