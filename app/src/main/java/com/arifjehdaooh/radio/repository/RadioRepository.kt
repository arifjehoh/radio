/*
 * Developed by Arif Jehda-Oh on 2019-06-14 14:54.
 * Last modified 2019-06-14 12:25.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.arifjehdaooh.radio.api.NetworkService
import com.arifjehdaooh.radio.api.entity.ChannelEntity
import com.arifjehdaooh.radio.api.entity.ChannelsEntity
import com.arifjehdaooh.radio.api.entity.ExampleEntity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class RadioRepository @Inject constructor(private val apiService: NetworkService) {

    //fun getExampleDogs(): LiveData<Resource<ExampleEntity>> {
    //    val liveData: MutableLiveData<Resource<ExampleEntity>> = MutableLiveData()
    //    liveData.postValue(Resource.loading())
    //    apiService
    //        .getSomeExampleDogs()
    //        .enqueue(object : Callback<ExampleEntity> {
    //            override fun onFailure(call: Call<ExampleEntity>, t: Throwable) {
    //                liveData.postValue(Resource.error(t))
    //            }
//
    //            override fun onResponse(call: Call<ExampleEntity>, response: Response<ExampleEntity>) {
    //                if (response.isSuccessful && response.body() != null) {
    //                    liveData.postValue(Resource.success(response.body()!!))
    //                } else {
    //                    liveData.postValue(Resource.error(response))
    //                }
    //            }
    //        })
    //    return liveData
    //}
    fun getChannels(): LiveData<Resource<ChannelsEntity>> {
        val liveData: MutableLiveData<Resource<ChannelsEntity>> = MutableLiveData()
        liveData.postValue(Resource.loading())
        apiService
            .getChannels()
            .enqueue(object : Callback<ChannelsEntity> {
                override fun onFailure(call: Call<ChannelsEntity>, t: Throwable) {
                    liveData.postValue(Resource.error(t))
                }

                override fun onResponse(call: Call<ChannelsEntity>, response: Response<ChannelsEntity>) {
                    if (response.isSuccessful && response.body() != null) {
                        liveData.postValue(Resource.success(response.body()!!))
                    } else {
                        liveData.postValue(Resource.error(response))
                    }
                }

            })
        return liveData
    }

    fun getNextPage(page: Int): LiveData<Resource<ChannelsEntity>> {
        val liveData: MutableLiveData<Resource<ChannelsEntity>> = MutableLiveData()
        liveData.postValue(Resource.loading())
        apiService
            .getNextPage(page.toString())
            .enqueue(object : Callback<ChannelsEntity> {
                override fun onFailure(call: Call<ChannelsEntity>, t: Throwable) {
                    liveData.postValue(Resource.error(t))
                }

                override fun onResponse(call: Call<ChannelsEntity>, response: Response<ChannelsEntity>) {
                    if (response.isSuccessful && response.body() != null) {
                        liveData.postValue(Resource.success(response.body()!!))
                    } else {
                        liveData.postValue(Resource.error(response))
                    }
                }

            })
        return liveData
    }

    fun getChannel(channel_id: Int): LiveData<Resource<ChannelEntity>> {
        val liveData: MutableLiveData<Resource<ChannelEntity>> = MutableLiveData()
        liveData.postValue(Resource.loading())
        apiService
            .getChannel(channel_id.toString())
            .enqueue(object : Callback<ChannelEntity> {
                override fun onFailure(call: Call<ChannelEntity>, t: Throwable) {
                    liveData.postValue(Resource.error(t))
                }

                override fun onResponse(call: Call<ChannelEntity>, response: Response<ChannelEntity>) {
                    if (response.isSuccessful && response.body() != null) {
                        liveData.postValue(Resource.success(response.body()!!))
                    } else {
                        liveData.postValue(Resource.error(response))
                    }
                }

            })
        return liveData


    }
}