/*
 * Developed by Arif Jehda-Oh on 2019-06-14 15:11.
 * Last modified 2019-06-14 15:11.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.api.entity

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import junit.framework.Assert.assertEquals
import okio.BufferedSource
import okio.buffer
import okio.source
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import timber.log.Timber
import java.io.IOException
import java.io.InputStream

@RunWith(AndroidJUnit4::class)
class ExampleEntityTest {
    //@Test
    //fun useAppContext() {
    //    // Context of the app under test.
    //    val appContext = InstrumentationRegistry.getInstrumentation().getTargetContext()
    //    assertEquals("com.arifjehdaooh.baseproject_kt", appContext.packageName)
    //}

    lateinit var entity: ExampleEntity

    private fun getBodyFromAssetFile(fileName: String): String {
        lateinit var body: String
        try {
            var inputStream: InputStream = InstrumentationRegistry.getInstrumentation().targetContext.assets.open(fileName)
            var bufferedSource: BufferedSource = inputStream.source().buffer()
            body = bufferedSource.readUtf8()
            bufferedSource.close()
        } catch (e: IOException) {
            Timber.e(e)
        }
        return body
    }

    @Before
    fun setUp() {
        var json = getBodyFromAssetFile("channels.json")
        println(json)

        var gson: Gson = GsonBuilder()
            //.registerTypeAdapter()
            .create()
        entity = gson.fromJson(json, ExampleEntity::class.java)

        checkNotNull(json)
    }

    @Test
    fun retrievedData_success() {
        assertEquals("success", entity.status)
        println(entity.message)
        //for (dog: Map.Entry<String, List<String>> in entity.message.entries) {
        //    if (dog.value.isNotEmpty()) {
        //        println(dog.value)
        //        println(dog.value[dog.value.lastIndex])
        //    }
        //}
    }
}