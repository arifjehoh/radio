/*
 * Developed by Arif Jehda-Oh on 2019-06-20 15:15.
 * Last modified 2019-06-20 15:15.
 * Copyright (c) 2019.
 */

package com.arifjehdaooh.radio.api.entity

import androidx.test.platform.app.InstrumentationRegistry
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okio.BufferedSource
import okio.buffer
import okio.source
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import timber.log.Timber
import java.io.IOException
import java.io.InputStream

class ChannelsEntityTest {
    lateinit var entity: ChannelsEntity

    private fun getBodyFromAssetFile(fileName: String): String {
        lateinit var body: String
        try {
            var inputStream: InputStream = InstrumentationRegistry.getInstrumentation().targetContext.assets.open(fileName)
            var bufferedSource: BufferedSource = inputStream.source().buffer()
            body = bufferedSource.readUtf8()
            bufferedSource.close()
        } catch (e: IOException) {
            Timber.e(e)
        }
        return body
    }

    @Before
    fun setUp() {
        var json = getBodyFromAssetFile("channels.json")
        println(json)

        var gson: Gson = GsonBuilder()
            //.registerTypeAdapter()
            .create()
        entity = gson.fromJson(json, ChannelsEntity::class.java)
    }

    // Todo Check if null or empty
    @Test
    fun entityIsNotNullOrEmpty_success() {
        assertNotNull(entity)
    }

}