# Radio App
This project is the first Kotlin developed project I have been working on since I completed my internship at Visma SPCS.  
This project uses the Radio API from Swedish Radio (Sveriges Radio)  
The inspiration for this project was to recreate the knowledge I learnt from my internship and use it for Kotlin instead of Java. 

## This project contains
* Dagger 2
* Retrofit
* Model-View-ViewModel (Android Architecture)
* Kotlin
* Picasso


### Future implementation
* Rx(Android, Java, Kotlin?) - 